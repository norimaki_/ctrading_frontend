import { Component, OnInit } from '@angular/core';
import { DataTable } from "app/data-table/data-table.model";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TradeDetailComponent } from 'app/shared/trade-detail/trade-detail.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
  currentTab = 'opened';
  openedTable = new DataTable();
  openedPositions = new DataTable();
  closedTable = new DataTable();
  closedPositions = new DataTable();
  translations = {
    exchange: '',
    market: '',
    type: '',
    amount: '',
    price: '',
    profit: '',
    openDate: '',
    percentage: ''
  }

  constructor(
    private _ngbModal: NgbModal,
    private _translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.setTranslations();
  }

  setTranslations(): void {
    this._translate.stream([
      'TABLE.HEADER.EXCHANGE',
      'TABLE.HEADER.MARKET',
      'TABLE.HEADER.TYPE',
      'TABLE.HEADER.AMOUNT',
      'TABLE.HEADER.PRICE',
      'TABLE.HEADER.PROFIT',
      'TABLE.HEADER.OPEN_DATE',
      'TABLE.HEADER.PERCENTAGE',
    ]).subscribe(translation => {
      this.translations = {
        exchange: translation['TABLE.HEADER.EXCHANGE'],
        market: translation['TABLE.HEADER.MARKET'],
        type: translation['TABLE.HEADER.TYPE'],
        amount: translation['TABLE.HEADER.AMOUNT'],
        price: translation['TABLE.HEADER.PRICE'],
        profit: translation['TABLE.HEADER.PROFIT'],
        openDate: translation['TABLE.HEADER.OPEN_DATE'],
        percentage: translation['TABLE.HEADER.PERCENTAGE']
      }
      this.fetchData();
    })
  }

  openTab(tab: string): void {
    this.currentTab = tab;
  }

  showTradeDetails(event: Event): void {
    const modalOptions = {
      name: event[0],
      open: event [6]
    };
    const modalRef: NgbModalRef = this._ngbModal.open(TradeDetailComponent);
    modalRef.componentInstance.trade = modalOptions;
  }

  fetchData(): void {
    setTimeout(() => {
      this.openedTable = {
        id: 'portfolio-opened',
        showHeader: true,
        tableClickable: true,
        tableHover: true,
        cols: [
          {
            type: 'string',
            name: this.translations.exchange,
            align: 'center'
          },
          {
            type: 'string',
            name: this.translations.market,
            align: 'center'
          },
          {
            type: 'label',
            name: this.translations.type,
            align: 'center',
            labelColors: {
              'BUY': 'success',
              'SELL': 'danger'
            }
          },
          {
            type: 'number',
            name: this.translations.amount,
            align: 'center'
          },
          {
            type: 'price',
            name: this.translations.price,
            currency: 'USD',
            align: 'center'
          },
          {
            type: 'percentage',
            name: `% ${this.translations.profit}`,
            align: 'center'
          },
          {
            type: 'date',
            name: this.translations.openDate,
            align: 'center'
          },
          {
            type: 'delete',
            name: '',
            align: 'center'
          }
        ],
        data: [
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 23:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 21:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 21:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+239', '2018-08-11 20:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'SELL', 13.83, 4.455, '+29', '2018-08-11 18:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'SELL', 13.83, 4.455, '+29', '2018-08-11 17:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+239', '2018-08-11 15:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-10 12:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-9 12:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'SELL', 13.83, 4.455, '+29', '2018-08-7 12:21:06', ''],
        ]
      };

      this.closedTable = {
        id: 'portfolio-closed',
        showHeader: true,
        cols: [
          {
            type: 'string',
            name: this.translations.exchange,
            align: 'center'
          },
          {
            type: 'string',
            name: this.translations.market,
            align: 'center'
          },
          {
            type: 'label',
            name: this.translations.type,
            align: 'center',
            labelColors: {
              'BUY': 'success',
              'SELL': 'danger'
            }
          },
          {
            type: 'number',
            name: this.translations.amount,
            align: 'center'
          },
          {
            type: 'price',
            name: this.translations.price,
            currency: 'USD',
            align: 'center'
          },
          {
            type: 'percentage',
            name: `% ${this.translations.profit}`,
            align: 'center'
          },
          {
            type: 'date',
            name: this.translations.openDate,
            align: 'center'
          }
        ],
        data: [
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+239', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'SELL', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'SELL', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+239', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'SELL', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
        ]
      };

      this.openedPositions = {
        id: 'opened-positions',
        showHeader: false,
        cols: [
          {
            type: 'string',
            name: this.translations.market
          },
          {
            type: 'percentage',
            name: this.translations.percentage,
            align: 'right'
          }
        ],
        data: [
          ['Simples', 50],
          ['Trailing Stop', 39],
          ['Arbitrage', 11]
        ],
        dataChart: {
          id: 'opened-positions',
          data: {
            labels: ['Simples', 'Trailing Stop', 'Arbitrage'],
            datasets: [{
              label: 'Exchanges',
              data: [50, 39, 11]
            }]
          },
          options: {
            legend: {
              display: false
            }
          }
        }
      };

      this.closedPositions = {
        id: 'closed-positions',
        showHeader: false,
        cols: [
          {
            type: 'string',
            name: this.translations.market
          },
          {
            type: 'percentage',
            name: this.translations.percentage,
            align: 'right'
          }
        ],
        data: [
          ['Simples', 24],
          ['Trailing Stop', 35],
          ['Arbitrage', 41]
        ],
        dataChart: {
          id: 'closed-positions',
          data: {
            labels: ['Simples', 'Trailing Stop', 'Arbitrage'],
            datasets: [{
              label: 'Exchanges',
              data: [24, 35, 41]
            }]
          },
          options: {
            legend: {
              display: false
            }
          }
        }
      };
    }, 500);
  }
}
