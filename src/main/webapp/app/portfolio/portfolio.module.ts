import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { portfolioState } from './portfolio.route';
import { PortfolioComponent } from './portfolio.component';
import { CtradingClientSharedModule } from "app/shared/shared.module";
import { DataTableModule } from "app/data-table/data-table.module";
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PortfolioComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(portfolioState),
    CtradingClientSharedModule,
    DataTableModule,
    TranslateModule
  ]
})
export class PortfolioModule { }
