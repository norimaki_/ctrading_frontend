import { Routes } from '@angular/router';
import { PortfolioComponent } from "./portfolio.component";

export const portfolioState: Routes = [
  {
    path: '',
    component: PortfolioComponent
  }
];
