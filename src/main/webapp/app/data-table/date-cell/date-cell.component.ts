import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-date-cell',
  templateUrl: './date-cell.component.html',
  styleUrls: ['./date-cell.component.scss']
})
export class DateCellComponent implements OnInit {
  @Input() column = {};
  @Input() data = '';

  constructor() {}

  ngOnInit(): void {
    if (this.column.format === undefined) {
      this.column.format = 'yyyy-MM-dd H:mm:ss';
    }
  }
}
