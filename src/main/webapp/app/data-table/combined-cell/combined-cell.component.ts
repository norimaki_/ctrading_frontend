import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-combined-cell',
  templateUrl: './combined-cell.component.html',
  styleUrls: ['./combined-cell.component.scss']
})
export class CombinedCellComponent implements OnInit {
  @Input() column = {};
  @Input() data = [];
  types = new Array(this.data.length);

  constructor() {}

  ngOnInit(): void {
    if (this.column.types === undefined) return;
    this.types = this.column.types;
  }
}
