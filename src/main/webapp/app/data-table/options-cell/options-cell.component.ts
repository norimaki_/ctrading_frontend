import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-options-cell',
  templateUrl: './options-cell.component.html',
  styleUrls: ['./options-cell.component.scss']
})
export class OptionsCellComponent implements OnInit {
  @Input() data = {};

  constructor() { }

  ngOnInit(): void {}

}
