import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-price-cell',
  templateUrl: './price-cell.component.html',
  styleUrls: ['./price-cell.component.scss']
})
export class PriceCellComponent implements OnInit {
  @Input() col = {};
  @Input() data = 0;

  constructor() {}

  ngOnInit(): void {}
}
