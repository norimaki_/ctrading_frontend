import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { DataTableComponent } from "./data-table.component";
import { StringCellComponent } from "./string-cell/string-cell.component";
import { PercentageCellComponent } from "./percentage-cell/percentage-cell.component";
import { IconCellComponent } from "./icon-cell/icon-cell.component";
import { OptionsCellComponent } from './options-cell/options-cell.component';
import { PriceCellComponent } from './price-cell/price-cell.component';
import { LabelCellComponent } from './label-cell/label-cell.component';
import { CombinedCellComponent } from './combined-cell/combined-cell.component';
import { ButtonCellComponent } from './button-cell/button-cell.component';
import { DateCellComponent } from './date-cell/date-cell.component';
import { DotCellComponent } from './dot-cell/dot-cell.component';

@NgModule({
  declarations: [
    DataTableComponent,
    StringCellComponent,
    PercentageCellComponent,
    IconCellComponent,
    OptionsCellComponent,
    PriceCellComponent,
    LabelCellComponent,
    CombinedCellComponent,
    ButtonCellComponent,
    DateCellComponent,
    DotCellComponent
  ],
  imports: [
    CommonModule,
    NgbModule
  ],
  exports: [
    DataTableComponent
  ]
})
export class DataTableModule { }
