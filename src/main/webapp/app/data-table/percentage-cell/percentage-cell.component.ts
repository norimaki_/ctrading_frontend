import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-percentage-cell',
  templateUrl: './percentage-cell.component.html',
  styleUrls: ['./percentage-cell.component.scss']
})
export class PercentageCellComponent implements OnInit {
  @Input() column = {};
  @Input() data = '';

  constructor() { }

  ngOnInit(): void {}
}
