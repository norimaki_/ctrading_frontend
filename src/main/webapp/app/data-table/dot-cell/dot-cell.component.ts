import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-dot-cell',
  templateUrl: './dot-cell.component.html',
  styleUrls: ['./dot-cell.component.scss']
})
export class DotCellComponent implements OnInit {
  @Input() color = '';

  constructor() { }

  ngOnInit(): void {}
}
