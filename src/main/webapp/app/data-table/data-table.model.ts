export interface IDataTable {
  id?: string,
  data?: any[],
  title?: string,
  rowsHeight?: string,
  titleAlign?: string,
  showHeader?: boolean,
  cols?: object[],
  dataChart?: object,
  tableHover?: boolean,
  tableClickable?: boolean
}

export class DataTable implements IDataTable {
  constructor(
    public id?: string,
    public data?: any[],
    public title?: string,
    public rowsHeight?: string,
    public titleAlign?: string,
    public showHeader?: boolean,
    public cols?: object[],
    public dataChart?: object,
    public tableHover?: boolean,
    public tableClickable?: boolean
  ) {}
}
