import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-button-cell',
  templateUrl: './button-cell.component.html',
  styleUrls: ['./button-cell.component.scss']
})
export class ButtonCellComponent implements OnInit {
  @Input() data = {};

  constructor() { }

  ngOnInit(): void {}
}
