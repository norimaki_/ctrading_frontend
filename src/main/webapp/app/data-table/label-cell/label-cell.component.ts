import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-label-cell',
  templateUrl: './label-cell.component.html',
  styleUrls: ['./label-cell.component.scss']
})
export class LabelCellComponent implements OnInit {
  @Input() col = {};
  @Input() data = '';

  constructor() { }

  ngOnInit(): void {}

}
