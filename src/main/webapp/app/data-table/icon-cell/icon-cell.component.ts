import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-icon-cell',
  templateUrl: './icon-cell.component.html',
  styleUrls: ['./icon-cell.component.scss']
})
export class IconCellComponent implements OnInit {
  @Input() data = "";

  constructor() { }

  ngOnInit(): void {
  }

}
