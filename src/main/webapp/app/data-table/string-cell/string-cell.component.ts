import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-string-cell',
  templateUrl: './string-cell.component.html',
  styleUrls: ['./string-cell.component.scss']
})
export class StringCellComponent implements OnInit {
  @Input() data = '';

  constructor() { }

  ngOnInit(): void {
  }

}
