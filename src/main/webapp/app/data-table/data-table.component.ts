import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DataTable } from "./data-table.model";
import { chartColors } from 'app/shared/chart/chart.model';

@Component({
  selector: 'jhi-data-table',
  templateUrl: './data-table.component.html'
})
export class DataTableComponent {
  @Input() dataTable = new DataTable();
  @Output() clickOutput: EventEmitter<any> = new EventEmitter();
  rowSelected = [];
  chartColors = chartColors;

  constructor() {}

  handleClick(row: []): void {
    if (this.dataTable.tableClickable) {
      this.clickOutput.emit(row);
      this.rowSelected = row;
    }
  }
}
