import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
// import { errorRoute } from './layouts/error/error.route';
// import { navbarRoute } from './layouts/navbar/navbar.route';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';
import { ControlPanelComponent } from "app/layouts/control-panel/control-panel.component";

// import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

// const LAYOUT_ROUTES = [navbarRoute, ...errorRoute];

const routes: Routes = [
  {
    path: '',
    component: ControlPanelComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'portfolio',
        loadChildren: () => import('./portfolio/portfolio.module').then(m => m.PortfolioModule)
      },
      {
        path: 'trading',
        loadChildren: () => import('./trading/trading.module').then(m => m.TradingModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)
      },
      {
        path: 'buy',
        loadChildren: () => import('./buy-sell/buy-sell.module').then(m => m.BuySellModule)
      },
      {
        path: 'arbitration',
        loadChildren: () => import('./arbitration/arbitration.module').then(m => m.ArbitrationModule)
      },
      {
        path: 'admin',
        data: {
          authorities: ['ROLE_ADMIN']
        },
        // canActivate: [UserRouteAccessService],
        loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule)
      }
    ]
  },
  {
    path: 'account',
    loadChildren: () => import('./account/account.module').then(m => m.AccountModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,{
      enableTracing: DEBUG_INFO_ENABLED,
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule]
})
export class CtradingClientAppRoutingModule {}
