import { Component, OnInit } from '@angular/core';
import { DataTable } from "app/data-table/data-table.model";

@Component({
  selector: 'jhi-arbitration',
  templateUrl: './arbitration.component.html',
  styleUrls: ['./arbitration.component.scss']
})
export class ArbitrationComponent implements OnInit {
  currentTab = 'closedArbitration';
  arbitrationTable = new DataTable();
  closedTable = new DataTable();
  profitsTable = new DataTable();
  profitsChart24h = {};
  profitsChart7d = {};
  profitsChart30d = {};

  constructor() { }

  ngOnInit(): void {
    this.fetchData();
  }

  openTab(tab: string): void {
    this.currentTab = tab;
  }

  fetchData(): void {
    setTimeout(() => {
      this.arbitrationTable = {
        id: 'arbitration-table',
        showHeader: true,
        cols: [
          {
            type: 'string',
            name: 'Market'
          },
          {
            type: 'number',
            name: 'Available',
            align: 'center'
          },
          {
            type: 'combined',
            name: 'Buy'
          },
          {
            type: 'combined',
            name: 'Sell'
          },
          {
            type: 'number',
            name: 'Vol. MIN',
            align: 'center'
          },
          {
            type: 'number',
            name: 'Vol. MAX',
            align: 'center'
          },
          {
            type: 'combined',
            types: ['percentage', 'price'],
            name: '% Profit',
            align: 'center'
          },
          {
            type: 'percentage',
            name: 'Balance to Use',
            progressbar: true,
            align: 'center'
          },
          {
            type: 'button',
            name: '',
            align: 'right'
          }
        ],
        data: [
          ['ETH-XMR', 13.83, ['Bitfinex', 0.0270], ['Hitbtc', 0.0270], 1234, 5678, ['+29', 0.56], 47.2, {label: 'Arbitrage ETH-XMR', action: ''}],
          ['BTC-XRP', 13.83, ['Poloniex', 0.0270], ['Hitbtc', 0.0270], 1234, 5678, ['+35', 0.26], 15.3, {label: 'Arbitrage BTC-XRP', action: ''}],
          ['BTC-LTC', 0, ['Poloniex', 0.0380], ['Hitbtc', 0.0270], 1234, 5678, ['-0.9', 45.56], 67.8, {label: 'Arbitrage BTC-LTC', action: ''}],
          ['ETH-XMR', 13.83, ['Bitfinex', 0.0270], ['Hitbtc', 0.0270], 1234, 5678, ['+29', 0.56], 47.2, {label: 'Arbitrage ETH-XMR', action: ''}],
          ['BTC-XRP', 13.83, ['Poloniex', 0.0270], ['Hitbtc', 0.0270], 1234, 5678, ['-35', 0.26], 15.3, {label: 'Arbitrage BTC-XRP', action: ''}],
          ['BTC-LTC', 0, ['Poloniex', 0.0380], ['Hitbtc', 0.0270], 1234, 5678, ['+0.9', 45.56], 67.8, {label: 'Arbitrage BTC-LTC', action: ''}],
          ['ETH-XMR', 13.83, ['Bitfinex', 0.0270], ['Hitbtc', 0.0270], 1234, 5678, ['-29', 0.56], 47.2, {label: 'Arbitrage ETH-XMR', action: ''}],
          ['BTC-XRP', 13.83, ['Poloniex', 0.0270], ['Hitbtc', 0.0270], 1234, 5678, ['+35', 0.26], 15.3, {label: 'Arbitrage BTC-XRP', action: ''}],
          ['BTC-LTC', 0, ['Poloniex', 0.0380], ['Hitbtc', 0.0270], 1234, 5678, ['+0.9', 45.56], 67.8, {label: 'Arbitrage BTC-LTC', action: ''}]
        ]
      }

      this.closedTable = {
        id: 'closed-table',
        showHeader: true,
        cols: [
          {
            type: 'string',
            name: 'Market'
          },
          {
            type: 'combined',
            types: ['percentage', 'price'],
            name: '% Profit',
            align: 'center'
          },
          {
            type: 'combined',
            name: 'Buy'
          },
          {
            type: 'combined',
            name: 'Sell'
          },
          {
            type: 'string',
            name: 'Date',
            align: 'center'
          }
        ],
        data: [
          ['ETH-XMR', ['+29', 10.64], ['Bitfinex', 0.0270], ['Hitbtc', 0.0270], '2018-08-11 12:21:06'],
          ['BTC-XMR', ['+50', 230.56], ['Bitfinex', 0.0270], ['Hitbtc', 0.0270], '2018-08-11 12:21:06'],
          ['ETH-LTC', ['+20', 0.56], ['Bitfinex', 0.0270], ['Hitbtc', 0.0270], '2018-08-11 12:21:06']
        ]
      };

      this.profitsTable = {
        id: 'profits-table',
        cols: [
          {
            type: 'string',
            name: 'Market'
          },
          {
            type: 'percentage',
            name: 'Percentage'
          }
        ],
        data: [
          ['BTC-XMR', 35],
          ['ETH-XMR', 30],
          ['ETH-LTC', 20],
          ['ETH-TRX', 15]
        ],
        dataChart: {
          id: 'profits-table',
          type: 'doughnut',
          data: {
            labels: ['BTC-XMR', 'ETH-XMR', 'ETH-LTC', 'ETH-TRX'],
            datasets: [{
              label: 'Exchanges',
              data: [35, 30, 20, 15],
              backgroundColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgb(232, 232, 232, 1)'
              ],
              borderColor: [
                'rgba(255, 255, 255, 1)',
                'rgba(255, 255, 255, 1)',
                'rgba(255, 255, 255, 1)'
              ],
              borderWidth: 2
            }]
          },
          options: {
            legend: {
              display: false
            }
          }
        }
      }

      this.profitsChart24h = {
        id: 'profits-24h',
        type: 'line',
        data: {
          labels: ['BTC-XMR', 'ETH-XMR', 'ETH-LTC', 'ETH-TRX'],
          datasets: [{
            label: 'Exchanges',
            data: [35, 3, 20, 15],
            fill: false,
            showLine: true,
            backgroundColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 2
          }]
        },
        options: {

        }
      }

      this.profitsChart7d = {
        id: 'profits-7d',
        type: 'line',
        data: {
          labels: ['BTC-XMR', 'ETH-XMR', 'ETH-LTC', 'ETH-TRX'],
          datasets: [{
            label: 'Exchanges',
            data: [35, 5, 70, 120],
            fill: false,
            showLine: true,
            backgroundColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 2
          }]
        },
        options: {

        }
      }

      this.profitsChart30d = {
        id: 'profits-30d',
        type: 'line',
        data: {
          labels: ['BTC-XMR', 'ETH-XMR', 'ETH-LTC', 'ETH-TRX'],
          datasets: [{
            label: 'Exchanges',
            data: [10, 15, 70, 64],
            fill: false,
            showLine: true,
            backgroundColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 2
          }]
        },
        options: {

        }
      }
    }, 1500);
  }
}
