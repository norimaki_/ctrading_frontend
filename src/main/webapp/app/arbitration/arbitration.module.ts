import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArbitrationComponent } from './arbitration.component';
import { RouterModule } from "@angular/router";
import { ArbitrationState } from "./arbitration.route";
import { CtradingClientSharedModule } from "app/shared/shared.module";
import { DataTableModule } from "app/data-table/data-table.module";


@NgModule({
  declarations: [ArbitrationComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ArbitrationState),
    CtradingClientSharedModule,
    DataTableModule
  ]
})
export class ArbitrationModule { }
