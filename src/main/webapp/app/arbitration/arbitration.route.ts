import { Routes } from '@angular/router';
import { ArbitrationComponent } from "./arbitration.component";

export const ArbitrationState: Routes = [
  {
    path: '',
    component: ArbitrationComponent
  }
];
