import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CtradingClientSharedModule } from 'app/shared/shared.module';

import { PasswordStrengthBarComponent } from './password/password-strength-bar.component';
import { RegisterComponent } from './register/register.component';
import { ActivateComponent } from './activate/activate.component';
import { PasswordComponent } from './password/password.component';
import { PasswordResetInitComponent } from './password-reset/init/password-reset-init.component';
import { PasswordResetFinishComponent } from './password-reset/finish/password-reset-finish.component';
import { SettingsComponent } from './settings/settings.component';
import { accountState } from './account.route';
import { LoginComponent } from "./login/login.component";
import { LoginButtonsComponent } from './login-buttons/login-buttons.component';
import { AccountComponent } from './account.component';

@NgModule({
  imports: [
    CtradingClientSharedModule,
    RouterModule.forChild(accountState)
  ],
  declarations: [
    ActivateComponent,
    RegisterComponent,
    PasswordComponent,
    PasswordStrengthBarComponent,
    PasswordResetInitComponent,
    PasswordResetFinishComponent,
    SettingsComponent,
    LoginComponent,
    LoginButtonsComponent,
    AccountComponent
  ]
})
export class AccountModule {}
