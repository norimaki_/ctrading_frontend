import { Route } from '@angular/router';

import { LoginButtonsComponent } from './login-buttons.component';

export const loginButtonsRoute: Route = {
  path: 'index',
  component: LoginButtonsComponent,
  data: {
    authorities: [],
    pageTitle: 'Login'
  }
};
