import { Routes } from '@angular/router';

import { activateRoute } from './activate/activate.route';
import { passwordRoute } from './password/password.route';
import { passwordResetFinishRoute } from './password-reset/finish/password-reset-finish.route';
import { passwordResetInitRoute } from './password-reset/init/password-reset-init.route';
import { registerRoute } from './register/register.route';
import { loginRoute } from './login/login.route';
import { loginButtonsRoute } from './login-buttons/login-buttons.route';
import { settingsRoute } from './settings/settings.route';
import { AccountComponent } from "./account.component";

const ACCOUNT_ROUTES = [activateRoute, passwordRoute, passwordResetFinishRoute, passwordResetInitRoute, registerRoute, loginRoute, loginButtonsRoute, settingsRoute];

export const accountState: Routes = [
  {
    path: '',
    component: AccountComponent,
    // redirectTo: 'account/index',
    // pathMatch: 'full',
    children: ACCOUNT_ROUTES
  }
];
