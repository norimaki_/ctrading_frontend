import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { profileState } from "./profile.route";
import { RouterModule } from "@angular/router";
import { CtradingClientSharedModule } from "app/shared/shared.module";
import { PersonalDataComponent } from './personal-data/personal-data.component';
import { ExchangeStatusComponent } from './exchange-status/exchange-status.component';
import { ApikeysFormComponent } from './apikeys-form/apikeys-form.component';

@NgModule({
  declarations: [ProfileComponent, PersonalDataComponent, ExchangeStatusComponent, ApikeysFormComponent],
  imports: [
    CommonModule,
    CtradingClientSharedModule,
    RouterModule.forChild(profileState)
  ]
})
export class ProfileModule { }
