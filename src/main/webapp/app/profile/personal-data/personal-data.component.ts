import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'jhi-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.scss']
})
export class PersonalDataComponent implements OnInit {
  profileForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254)]],
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    phone: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(12)]],
    customCheckTerms: ['', [Validators.requiredTrue]],
    customCheckNewsletter: ['', [Validators.requiredTrue]],
  });

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit():void {
  }

}
