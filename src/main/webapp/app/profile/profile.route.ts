import { Routes } from '@angular/router';
import { ProfileComponent } from "./profile.component";

export const profileState: Routes = [
  {
    path: '',
    component: ProfileComponent
  }
];
