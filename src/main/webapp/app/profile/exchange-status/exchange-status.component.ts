import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from "@angular/animations";
import { Exchange } from 'app/core/exchange/exchange.model';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { ExchangeService } from "app/core/exchange/exchange.service";


@Component({
  selector: 'jhi-exchange-status',
  templateUrl: './exchange-status.component.html',
  styleUrls: ['./exchange-status.component.scss'],
  animations: [
    trigger('openClose', [
      state('opened', style({
        height: '*', visibility: 'visible', marginTop: '30px', marginBottom: '30px'
      })),
      state('closed', style({
        height: '0', minHeight: '0', visibility: 'hidden', overflow: 'hidden', position: 'relative', marginTop: '0', marginBottom: '0'
      })),
      transition('opened <=> closed', [
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ])
    ])
  ]
})
export class ExchangeStatusComponent implements OnInit {
  enable = true;
  expandedElement: any = null;
  faTimes = faTimes;
  exchanges: Exchange[] = [];

  constructor(
    private exchangeService: ExchangeService
  ) { }

  ngOnInit(): void {
    this.getUserExchanges();
  }

  getUserExchanges(): void {
    this.exchanges = this.exchangeService.getUserExchanges();
    // window.console.log('this.exchanges', this.exchanges);
    // this.exchangeService.getUserExchanges().subscribe((_exchanges) => {
    //   this.exchanges = _exchanges;
    // });
  }

  exchangeActiveChange($event: Event): void {
    window.console.log('(valueChange) not working', $event);
    // this.exchangeService.setExchangeStatus()
  }

  toggleRow(index: number): void {
    this.expandedElement = (index === this.expandedElement) ? null : index;
  }
}
