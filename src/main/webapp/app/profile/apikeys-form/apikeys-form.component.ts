import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { Exchange } from 'app/core/exchange/exchange.model';
import { ExchangeService } from "app/core/exchange/exchange.service";

@Component({
  selector: 'jhi-apikeys-form',
  templateUrl: './apikeys-form.component.html'
})
export class ApikeysFormComponent implements OnInit {
  @Input() exchange: Exchange = {
    id: '',
    name: ''
  };

  exchangesForm = this.fb.group({
    publickey: [this.exchange.publicKey, Validators.required],
    privatekey: [this.exchange.privateKey, Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private exchangeService: ExchangeService
  ) { }

  ngOnInit(): void {}

  sendApiKeys($event: Event): void {
    $event.preventDefault();
    if (this.exchangesForm.valid) {
      const apiKeys = this.exchangesForm.value;
      this.exchangeService.updateApiKeys(this.exchange.id, apiKeys).subscribe(result => {
        window.console.log('result ****', result);
      }, (error => {
        window.console.log('error', error);
      }));
    }
  }
}
