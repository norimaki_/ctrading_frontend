import { Component, OnInit } from '@angular/core';
import { AccountService } from "app/core/auth/account.service.mock";
import { NgxPermissionsService, NgxRolesService } from "ngx-permissions";
import { Router } from "@angular/router";
import { Account } from "app/core/user/account.model";

@Component({
  selector: 'jhi-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: Account | null = null;

  constructor(
    private accountService: AccountService,
    private permissionsService: NgxPermissionsService,
    private rolesService: NgxRolesService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getAuth();
  }

  getAuth(): void {
    this.accountService.getAuthenticationState().subscribe((_user) => {
      if (_user === null) {
        this.router.navigate(['account/index']);
        return;
      }
      this.user = _user;
      this.permissionsService.loadPermissions(this.user.authorities);
    });
  }

}
