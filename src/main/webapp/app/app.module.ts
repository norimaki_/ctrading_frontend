import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import './vendor';
import { CtradingClientSharedModule } from 'app/shared/shared.module';
import { CtradingClientCoreModule } from 'app/core/core.module';
import { CtradingClientAppRoutingModule } from './app-routing.module';
import { CtradingClientHomeModule } from './home/home.module';
import { CtradingClientEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';
import { ControlPanelComponent } from './layouts/control-panel/control-panel.component';
import { NavbarComponent } from "app/shared/navbar/navbar.component";
import { DataTableModule } from "app/data-table/data-table.module";
import { ThemeModule } from 'app/theme/theme.module';

import { lightTheme } from 'app/theme/light-theme';
import { darkTheme } from 'app/theme/dark-theme';

// Translations
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CtradingClientSharedModule,
    CtradingClientCoreModule,
    CtradingClientHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    CtradingClientEntityModule,
    CtradingClientAppRoutingModule,
    DataTableModule,
    ThemeModule.forRoot({
      themes: [lightTheme, darkTheme],
      active: 'light'
    }),
    DataTableModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, '/content/i18n/', '.json'),
        deps: [HttpClient]
      }
    })
  ],
  declarations: [MainComponent, ErrorComponent, PageRibbonComponent, FooterComponent, ControlPanelComponent, NavbarComponent],
  bootstrap: [MainComponent]
})
export class CtradingClientAppModule {}

