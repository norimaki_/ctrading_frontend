import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuySellComponent } from './buy-sell.component';
import { RouterModule} from "@angular/router";
import { BuySellState } from "app/buy-sell/buy-sell.route";
import { CtradingClientSharedModule } from "app/shared/shared.module";
import { BuyComponent } from './buy/buy.component';
import { SellComponent } from './sell/sell.component';
import { DataTableModule } from "app/data-table/data-table.module";

@NgModule({
  declarations: [BuySellComponent, BuyComponent, SellComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(BuySellState),
    CtradingClientSharedModule,
    DataTableModule
  ]
})
export class BuySellModule { }
