import { Routes } from '@angular/router';
import { BuySellComponent } from "./buy-sell.component";

export const BuySellState: Routes = [
  {
    path: '',
    component: BuySellComponent
  }
];
