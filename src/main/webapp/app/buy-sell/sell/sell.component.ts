import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-sell',
  templateUrl: './sell.component.html',
  styleUrls: ['./sell.component.scss']
})
export class SellComponent implements OnInit {
  @Input() exchange: any;

  constructor() { }

  ngOnInit(): void {
  }

}
