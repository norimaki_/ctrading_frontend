import { Component, OnInit, HostListener } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { DataTable } from "app/data-table/data-table.model";
import { createChart } from "lightweight-charts";

@Component({
  selector: 'jhi-buy-sell',
  templateUrl: './buy-sell.component.html',
  styleUrls: ['./buy-sell.component.scss']
})
export class BuySellComponent implements OnInit {
  currentTab: String = 'buyTab';
  statusTab: String = 'opened';
  currentExchange = {
    id: '',
    name: ''
  };
  exchanges: any[] = [];
  buySellForm = this.fb.group({
    price: '',
    amount: [0, [Validators.required]],
    total: [0, [Validators.required]]
  });
  pairSelected = '';
  trailingStop = true;
  openedTable = new DataTable();
  closedTable = new DataTable();
  pairList = new DataTable();
  chart = {};
  lineSeries = null;

  constructor(
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.fetchData();
    this.loadTradingChart();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: Event): void {
    this.resizeCharts();
  }

  resizeCharts(): void {
    if (this.chart === null) return;
    const chartElement = document.getElementById('trading-chart');
    if (chartElement === null) return;
    this.lineSeries = this.chart.resize(chartElement.offsetWidth, 300, false);
  }

  loadTradingChart(): void {
    const chartElement = document.getElementById('trading-chart');
    if (chartElement === null) return;
    this.chart = createChart(chartElement, {
      width: chartElement.offsetWidth,
      height: 300
    });
    this.lineSeries = this.chart.addLineSeries();
    this.lineSeries.setData([
      { time: '2013-01-17', value: 46.63 },
      { time: '2013-02-18', value: 76.64 },
      { time: '2013-03-19', value: 61.89 },
      { time: '2013-04-20', value: 74.43 },
      { time: '2013-05-09', value: 30.01 },
      { time: '2013-08-12', value: 56.63 },
      { time: '2013-09-13', value: 66.64 },
      { time: '2013-10-14', value: 81.89 },
      { time: '2013-11-15', value: 74.43 },
      { time: '2013-12-16', value: 80.01 },
      { time: '2014-01-17', value: 36.63 },
      { time: '2014-02-18', value: 76.64 },
      { time: '2014-03-19', value: 81.89 },
      { time: '2014-04-20', value: 74.43 },
      { time: '2014-05-09', value: 80.01 },
      { time: '2014-08-12', value: 16.63 },
      { time: '2014-09-13', value: 36.64 },
      { time: '2014-10-14', value: 41.89 },
      { time: '2014-11-15', value: 54.43 },
      { time: '2014-12-16', value: 40.01 },
      { time: '2015-01-17', value: 36.63 },
      { time: '2015-02-18', value: 66.64 },
      { time: '2015-03-19', value: 51.89 },
      { time: '2015-04-20', value: 74.43 },
      { time: '2015-05-09', value: 60.01 },
      { time: '2015-08-12', value: 56.63 },
      { time: '2015-09-13', value: 46.64 },
      { time: '2015-10-14', value: 31.89 },
      { time: '2015-11-15', value: 54.43 },
      { time: '2015-12-16', value: 40.01 },
      { time: '2016-01-17', value: 66.63 },
      { time: '2016-02-18', value: 76.64 },
      { time: '2016-03-19', value: 81.89 },
      { time: '2016-04-20', value: 94.43 },
      { time: '2016-05-09', value: 90.01 },
      { time: '2016-08-12', value: 96.63 },
      { time: '2016-09-13', value: 76.64 },
      { time: '2016-10-14', value: 81.89 },
      { time: '2016-11-15', value: 74.43 },
      { time: '2016-12-16', value: 80.01 },
      { time: '2017-01-17', value: 96.63 },
      { time: '2017-02-18', value: 86.64 },
      { time: '2017-03-19', value: 81.89 },
      { time: '2017-04-20', value: 84.43 },
      { time: '2017-05-09', value: 80.01 },
      { time: '2017-08-12', value: 76.63 },
      { time: '2017-09-13', value: 66.64 },
      { time: '2017-10-14', value: 51.89 },
      { time: '2017-11-15', value: 44.43 },
      { time: '2017-12-16', value: 30.01 },
      { time: '2018-01-17', value: 46.63 },
      { time: '2018-02-18', value: 76.64 },
      { time: '2018-03-19', value: 61.89 },
      { time: '2018-04-20', value: 74.43 },
      { time: '2018-05-09', value: 60.01 },
      { time: '2018-08-12', value: 56.63 },
      { time: '2018-09-13', value: 46.64 },
      { time: '2018-10-14', value: 61.89 },
      { time: '2018-11-15', value: 54.43 },
      { time: '2018-12-16', value: 40.01 },
      { time: '2019-01-17', value: 56.63 },
      { time: '2019-02-18', value: 66.64 },
      { time: '2019-03-19', value: 71.89 },
      { time: '2019-04-20', value: 74.43 },
    ]);
  }

  openTab(tab: string): void {
    this.currentTab = tab;
  }

  statusTabSwitch(tab: string): void {
    this.statusTab = tab;
  }

  dataChanged($event: Event): void {
    if ($event.target.value === null) return;
    this.currentExchange = this.exchanges.find(_exchange => _exchange.id === $event.target.value);
  }

  setPair(pair: string): void {
    this.pairSelected = pair[0];
  }

  buySellSubmit(): void {
    if (this.currentTab === 'buyTab') {
      window.console.log('BUY Submit');
    } else {
      window.console.log('SELL Submit');
    }
  }

  fetchData(): void {
    this.exchanges = [
      {
        id: 'bitcoin',
        name: 'Bitcoin'
      },
      {
        id: 'bitcoinSatos',
        name: 'BitcoinSatos...'
      },
      {
        id: 'bitcoinCash',
        name: 'Bitcoin Cash'
      },
      {
        id: 'ethereum',
        name: 'Ethereum'
      },
      {
        id: 'ripple',
        name: 'Ripple'
      },
      {
        id: 'litecoin',
        name: 'Litecoin'
      }
    ];
    this.currentExchange = this.exchanges[0];

    setTimeout(() => {
      this.openedTable = {
        id: 'portfolio-opened',
        showHeader: true,
        cols: [
          {
            type: 'string',
            name: 'Exchange',
            align: 'center'
          },
          {
            type: 'string',
            name: 'Market',
            align: 'center'
          },
          {
            type: 'label',
            name: 'Type',
            align: 'center',
            labelColors: {
              'BUY': 'success',
              'SELL': 'danger'
            }
          },
          {
            type: 'number',
            name: 'Amount',
            align: 'center'
          },
          {
            type: 'price',
            name: 'Price',
            currency: 'USD',
            align: 'center'
          },
          {
            type: 'percentage',
            name: '% Profit',
            align: 'center'
          },
          {
            type: 'date',
            name: 'Open Date',
            align: 'center'
          },
          {
            type: 'delete',
            name: 'Cancel',
            align: 'center'
          }
        ],
        data: [
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+239', '2018-08-11 12:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'SELL', 13.83, 4.455, '+29', '2018-08-11 12:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'SELL', 13.83, 4.455, '+29', '2018-08-11 12:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+239', '2018-08-11 12:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06', ''],
          ['Bitfinex', 'ETH-XMR', 'SELL', 13.83, 4.455, '+29', '2018-08-11 12:21:06', ''],
        ]
      };

      this.closedTable = {
        id: 'portfolio-closed',
        showHeader: true,
        cols: [
          {
            type: 'string',
            name: 'Exchange',
            align: 'center'
          },
          {
            type: 'string',
            name: 'Market',
            align: 'center'
          },
          {
            type: 'label',
            name: 'Type',
            align: 'center',
            labelColors: {
              'BUY': 'success',
              'SELL': 'danger'
            }
          },
          {
            type: 'number',
            name: 'Amount',
            align: 'center'
          },
          {
            type: 'price',
            name: 'Price',
            currency: 'USD',
            align: 'center'
          },
          {
            type: 'percentage',
            name: '% Profit',
            align: 'center'
          },
          {
            type: 'date',
            name: 'Open Date',
            align: 'center'
          }
        ],
        data: [
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+239', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'SELL', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'SELL', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+239', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'BUY', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
          ['Bitfinex', 'ETH-XMR', 'SELL', 13.83, 4.455, '+29', '2018-08-11 12:21:06'],
        ]
      };

      this.pairList = {
        id: 'pair-list',
        showHeader: true,
        rowsHeight: 'sm',
        tableClickable: true,
        tableHover: true,
        cols: [
          {
            type: 'string',
            name: 'Pairs',
            align: 'center'
          },
          {
            type: 'number',
            name: 'Amount',
            align: 'center'
          },
          {
            type: 'price',
            name: 'Price',
            align: 'center'
          }
        ],
        data: [
          ['XMR/BTC',	0.3233,	4455],
          ['LTC/BTC',	0.3233,	4455],
          ['ETH/BTC',	0.3233,	4455],
          ['BNB/BTC',	0.3233,	4455]
        ]
      };
    }, 500);
  }
}
