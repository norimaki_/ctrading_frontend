import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.scss']
})
export class BuyComponent implements OnInit {
  @Input() exchange: any;

  constructor() { }

  ngOnInit(): void {
  }

}
