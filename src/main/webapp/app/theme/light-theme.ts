import { Theme } from './theme.model';

export const lightTheme: Theme = {
  name: 'light',
  properties: {
    '--background': '#e9e9e9',
    '--on-background': '#000000',
    '--card-background': '#ffffff',
    '--card-border': '#dcdcdc', // #eaedf0
    '--tabs-color': '#495057',
    '--tabs-inactive-color': '#ffffff80',
    '--input-border-color': '#ced4da',
    '--primary': '#1976d2',
    '--on-primary': '#00000080',
    '--headlines': '#1874b3'
  }
};
