import { Directive, OnInit, ElementRef } from '@angular/core';
import { ThemeService } from './theme.service';
import { Theme } from './theme.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[jhiTheme]'
})
export class ThemeDirective implements OnInit {
  private unsubscribe = new Subject();

  constructor(
    private _themeService: ThemeService,
    private _elementRef: ElementRef
  ) { }

  ngOnInit(): void {
    const active = this._themeService.getActiveTheme();
    if (active) {
      this.updateTheme(active);
    }
    this._themeService.themeChange.pipe(takeUntil(this.unsubscribe))
      .subscribe((theme: Theme) => this.updateTheme(theme));
  }

  updateTheme(theme: Theme): void {
    // eslint-disable-next-line guard-for-in
    for (const key in theme.properties) {
      this._elementRef.nativeElement.style.setProperty(key, theme.properties[key]);
    }
  }
}
