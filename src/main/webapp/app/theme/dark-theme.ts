import { Theme } from './theme.model';

export const darkTheme: Theme = {
  name: 'dark',
  properties: {
    '--background': '#1F2125',
    '--on-background': '#ffffff',
    '--card-background': '#2c2d31',
    '--card-border': '#404952',
    '--tabs-color': '#9fa3a7',
    '--tabs-inactive-color': '#33383e',
    '--input-border-color': '#404952',
    '--primary': '#2c2d31',
    '--on-primary': '#777777',
    '--headlines': '#3398de'
  }
}
