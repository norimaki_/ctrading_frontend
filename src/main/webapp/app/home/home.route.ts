import { Route } from '@angular/router';

export const HOME_ROUTE: Route = {
  path: '',
  redirectTo: '/dashboard',
  pathMatch: 'full',
  data: {
    authorities: [],
    pageTitle: 'Welcome, Java Hipster!'
  }
};
