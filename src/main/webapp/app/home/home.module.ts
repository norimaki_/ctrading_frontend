import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CtradingClientSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
// import { HomeComponent } from './home.component';

@NgModule({
  imports: [CtradingClientSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: []
})
export class CtradingClientHomeModule {}
