import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Pagination } from 'app/shared/util/request-util';
import { IUser } from './user.model';

@Injectable({ providedIn: 'root' })
export class UserService {
  public resourceUrl = SERVER_API_URL + 'api/users';

  constructor(private http: HttpClient) {}

  create(user: IUser): Observable<IUser> {
    return this.http.post<IUser>(this.resourceUrl, user);
  }

  update(user: IUser): Observable<IUser> {
    return this.http.put<IUser>(this.resourceUrl, user);
  }

  find(login: string): Observable<IUser> {
    return this.http.get<IUser>(`${this.resourceUrl}/${login}`);
  }

  query(req?: Pagination): Observable<HttpResponse<IUser[]>> {
    const options = createRequestOption(req);
    const users = new Observable<HttpResponse<IUser[]>>(observer => {
      setTimeout(() => observer.next({
        headers: {
          'X-Total-Count': '20',
        },
        body: [
        {
          id: '0001',
          login: 'Britus',
          email: 'dbrito@exceptia.co',
          createdDate: new Date('Mon Mar 06 2020 13:00:36 GMT+0200'),
          avatar: 'https://static.abc.es/media/play/2017/09/28/avatar-kVmB--1240x698@abc.jpeg',
          authorities: ['customer'],
          activated: true
        },
        {
          id: '0002',
          login: 'Mike',
          email: 'mlopez@exceptia.co',
          createdDate: new Date('Mon Mar 06 2020 13:00:36 GMT+0200'),
          avatar: 'https://static.abc.es/media/play/2017/09/28/avatar-kVmB--1240x698@abc.jpeg',
          authorities: ['customer'],
          activated: false
        },
        {
          id: '0003',
          login: 'Erone',
          email: 'epintor@exceptia.co',
          createdDate: new Date('Mon Mar 06 2020 13:00:36 GMT+0200'),
          avatar: 'https://static.abc.es/media/play/2017/09/28/avatar-kVmB--1240x698@abc.jpeg',
          authorities: ['Trader'],
          activated: true
        },
        {
          id: '0004',
          login: 'Carlos',
          email: 'carlos@exceptia.co',
          createdDate: new Date('Mon Mar 06 2020 13:00:36 GMT+0200'),
          avatar: 'https://static.abc.es/media/play/2017/09/28/avatar-kVmB--1240x698@abc.jpeg',
          authorities: ['customer'],
          activated: false
        }]
      }), 1000);
    });
    return users;
    // return this.http.get<IUser[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(login: string): Observable<{}> {
    return this.http.delete(`${this.resourceUrl}/${login}`);
  }

  authorities(): Observable<string[]> {
    return this.http.get<string[]>(SERVER_API_URL + 'api/users/authorities');
  }
}
