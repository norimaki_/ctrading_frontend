export interface Exchange {
  id: string;
  name: string;
  status?: string;
  statusColor?: string;
  active?: boolean;
  publicKey?: string;
  privateKey?: string;
  createdDate?: string;
}
