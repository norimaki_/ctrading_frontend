import { Injectable } from '@angular/core';
import { Exchange } from 'app/core/exchange/exchange.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { SERVER_API_URL } from 'app/app.constants';

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {
  public resourceUrl = SERVER_API_URL + 'api/exchanges';

  constructor(
    private http: HttpClient
  ) {}

  getUserExchanges(): Exchange[] {
    const exchanges: Exchange[] = [
      {
        id: 'binance',
        name: 'Binance',
        status: 'active',
        statusColor: '#00d983',
        publicKey: 'AbCdEfGhI',
        privateKey: 'PoIuYtReWq',
        active: true,
        createdDate: '2018-08-11 12:21:06'
      },
      {
        id: 'bittrex',
        name: 'Bittrex',
        status: 'active',
        statusColor: '#00d983',
        publicKey: '',
        privateKey: '',
        active: false,
        createdDate: '2018-08-11 12:21:06'
      },
      {
        id: 'hitbtc',
        name: 'Hitbtc',
        status: 'active',
        statusColor: '#00d983',
        publicKey: 'AbCdEfGhI',
        privateKey: 'PoIuYtReWq',
        active: true,
        createdDate: '2018-08-11 12:21:06'
      },
      {
        id: 'poloniex',
        name: 'Poloniex',
        status: 'desactivated',
        statusColor: '#e4e6f1',
        publicKey: 'AbCdEfGhI',
        privateKey: 'PoIuYtReWq',
        active: false,
        createdDate: '2018-08-11 12:21:06'
      },
      {
        id: 'bitfinex',
        name: 'Bitfinex',
        status: 'active',
        statusColor: '#00d983',
        publicKey: '',
        privateKey: '',
        active: true,
        createdDate: '2018-08-11 12:21:06'
      },
      {
        id: 'gemini',
        name: 'Gemini',
        status: 'unabled',
        statusColor: '#e55541',
        publicKey: '',
        privateKey: '',
        active: false,
        createdDate: '2018-08-11 12:21:06'
      },
      {
        id: 'gemini2',
        name: 'Gemini 2',
        status: 'active',
        statusColor: '#00d983',
        publicKey: 'AbCdEfGhI',
        privateKey: 'PoIuYtReWq',
        active: true,
        createdDate: '2018-08-11 12:21:06'
      },
      {
        id: 'bitmex',
        name: 'Bitmex',
        status: 'active',
        statusColor: '#00d983',
        publicKey: 'AbCdEfGhI',
        privateKey: 'PoIuYtReWq',
        active: false,
        createdDate: '2018-08-11 12:21:06'
      },
      {
        id: 'kraken',
        name: 'Kraken',
        status: 'active',
        statusColor: '#00d983',
        publicKey: '',
        privateKey: '',
        active: true,
        createdDate: '2018-08-11 12:21:06'
      },
      {
        id: 'coinbasepro',
        name: 'Coinbase Pro',
        status: 'active',
        statusColor: '#00d983',
        publicKey: '',
        privateKey: '',
        active: false,
        createdDate: '2018-08-11 12:21:06'
      },
      {
        id: 'exchangen-2',
        name: 'Exchange N-2',
        status: 'active',
        statusColor: '#00d983',
        publicKey: '',
        privateKey: '',
        active: true,
        createdDate: '2018-08-11 12:21:06'
      },
      {
        id: 'Exchangen-1',
        name: 'Exchange N-1',
        status: 'unabled',
        statusColor: '#e55541',
        publicKey: '',
        privateKey: '',
        active: false,
        createdDate: '2018-08-11 12:21:06'
      },
      {
        id: 'Exchangen',
        name: 'Exchange N',
        status: 'desactivated',
        statusColor: '#e4e6f1',
        publicKey: 'AbCdEfGhI',
        privateKey: 'PoIuYtReWq',
        active: true,
        createdDate: '2018-08-11 12:21:06'
      },
    ];
    return exchanges;
  }

  setExchangeStatus(): void {
    window.console.log('HEY');
  }

  updateApiKeys(exchange: string, changes: Partial<Exchange>): Observable<Object> {
    return this.http.put(`${ this.resourceUrl }/${exchange}`, changes);
  }
}
