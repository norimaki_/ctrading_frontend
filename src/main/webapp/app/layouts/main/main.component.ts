import { Component, HostBinding, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRouteSnapshot, NavigationEnd, NavigationError } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { Account } from "app/core/user/account.model";
import { AccountService } from 'app/core/auth/account.service.mock';
import { ThemeService } from 'app/theme/theme.service';
import { takeUntil } from 'rxjs/operators';
import { Theme } from 'app/theme/theme.model';

@Component({
  selector: 'jhi-main',
  templateUrl: './main.component.html'
})
export class MainComponent implements OnInit {
  @HostBinding('class.dark-mode') darkMode = false;
  private user: Account = new Account();
  private unsubscribe = new Subject();

  constructor(
    private accountService: AccountService,
    private titleService: Title,
    private router: Router,
    private themeService: ThemeService
  ) {}

  ngOnInit(): void {
    const subject = new BehaviorSubject<Account>(null);
    // try to log in automatically
    this.accountService.identity().subscribe((_account) => {
      this.user = _account;
      window.console.log('this.user ********', this.user );
      subject.subscribe({
        next: (v) => window.console.log('v ***********', v)
      });
      subject.next(this.user);
      // window.console.log('subject', subject);
    });

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.updateTitle();
      }
      if (event instanceof NavigationError && event.error.status === 404) {
        this.router.navigate(['/404']);
      }
    });

    const active = this.themeService.getActiveTheme();
    this.darkMode = (active.name === 'dark') ? true : false;
    this.themeService.themeChange.pipe(takeUntil(this.unsubscribe)).subscribe((theme: Theme) => {
      this.darkMode = (theme.name === 'dark') ? true : false;
    });
  }

  private getPageTitle(routeSnapshot: ActivatedRouteSnapshot): string {
    let title: string = routeSnapshot.data && routeSnapshot.data['pageTitle'] ? routeSnapshot.data['pageTitle'] : '';
    if (routeSnapshot.firstChild) {
      title = this.getPageTitle(routeSnapshot.firstChild) || title;
    }
    return title;
  }

  private updateTitle(): void {
    let pageTitle = this.getPageTitle(this.router.routerState.snapshot.root);
    if (!pageTitle) {
      pageTitle = 'Ctrading_client';
    }
    this.titleService.setTitle(pageTitle);
  }
}
