import { Routes } from '@angular/router';
import { UsersListComponent } from "./users-list/users-list.component";

export const usersState: Routes = [
  {
    path: '',
    redirectTo: 'list'
  },
  {
    path: 'list',
    component: UsersListComponent
  }
];
