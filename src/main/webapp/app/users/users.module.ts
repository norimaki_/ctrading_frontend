import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { CtradingClientSharedModule } from "app/shared/shared.module";
import { usersState } from "./users.route";
import { RouterModule } from "@angular/router";
import { UsersListComponent } from './users-list/users-list.component';
import { DataTableModule } from "app/data-table/data-table.module";

@NgModule({
  declarations: [UsersComponent, UsersListComponent],
  imports: [
    CommonModule,
    CtradingClientSharedModule,
    RouterModule.forChild(usersState),
    DataTableModule
  ]
})
export class UsersModule { }
