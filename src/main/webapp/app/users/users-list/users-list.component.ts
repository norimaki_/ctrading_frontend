import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jhi-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  public users = [
    {
      id: '0001',
      login: 'Britus',
      email: 'dbrito@exceptia.co',
      createdDate: '2020-04-07',
      useravatar: 'https://static.abc.es/media/play/2017/09/28/avatar-kVmB--1240x698@abc.jpeg',
      role: 'customer'
    },
    {
      id: '0002',
      login: 'Mike',
      email: 'mlopez@exceptia.co',
      createdDate: '2020-04-07',
      useravatar: 'https://static.abc.es/media/play/2017/09/28/avatar-kVmB--1240x698@abc.jpeg',
      role: 'customer'
    },
    {
      id: '0003',
      login: 'Erone',
      email: 'epintor@exceptia.co',
      createdDate: '2020-04-07',
      useravatar: 'https://static.abc.es/media/play/2017/09/28/avatar-kVmB--1240x698@abc.jpeg',
      role: 'Trader'
    },
    {
      id: '0004',
      login: 'Carlos',
      email: 'carlos@exceptia.co',
      createdDate: '2020-04-07',
      useravatar: 'https://static.abc.es/media/play/2017/09/28/avatar-kVmB--1240x698@abc.jpeg',
      role: 'customer'
    }
  ];

  public usersTable = {
    id: 'users-table',
    showHeader: true,
    cols: [
      {
        type: 'icon',
        name: ''
      },
      {
        type: 'string',
        name: 'User'
      },
      {
        type: 'label',
        name: 'Role',
        align: 'center'
      },
      {
        type: 'options',
        name: '',
        align: 'right'
      }
    ],
    data: []
  };

  constructor() {}

  ngOnInit(): void {
    this.setUsersTable();
  }

  setUsersTable(): void {
    this.users.forEach(_user => {
      window.console.log('_user', _user);
      const userRow: object = [_user.useravatar, _user.username, _user.role, {'View User': _user.id}];
      this.usersTable.data.push(userRow);
    });
  }
}
