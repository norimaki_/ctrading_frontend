import { AfterViewInit, Component, ElementRef, Renderer, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
// import { LoginService } from "app/core/login/login.service";
import { LoginService } from "app/core/login/login.service.mock";
import { Router } from "@angular/router";

@Component({
  selector: 'jhi-login-form',
  templateUrl: './login-form.component.html'
})
export class LoginFormComponent implements AfterViewInit {
  @ViewChild('username', { static: false })
  username?: ElementRef;

  authenticationError = false;

  loginForm = this.fb.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]],
    rememberMe: [false]
  });

  constructor(
    private loginService: LoginService,
    private renderer: Renderer,
    private router: Router,
    private fb: FormBuilder,
    // public activeModal: NgbActiveModal,
  ) { }

  ngAfterViewInit(): void {
    if (this.username) {
      this.renderer.invokeElementMethod(this.username.nativeElement, 'focus', []);
    }
  }

  login(): void {
    this.loginService
      .login({
        username: this.loginForm.get('username')!.value,
        password: this.loginForm.get('password')!.value,
        rememberMe: this.loginForm.get('rememberMe')!.value
      })
      .subscribe(
        () => {
          window.console.log('TODO BIEN');
          this.authenticationError = false;
          if (
            this.router.url === '/account/login' ||
            this.router.url === '/account/register' ||
            this.router.url.startsWith('/account/activate') ||
            this.router.url.startsWith('/account/reset/')
          ) {
            this.router.navigate(['']);
          }
        },
        () => {
          this.authenticationError = true
        }
      );
  }

  register(): void {
    // this.activeModal.dismiss('to state register');
    this.router.navigate(['/account/register']);
  }

  requestResetPassword(): void {
    // this.activeModal.dismiss('to state requestReset');
    this.router.navigate(['/account/reset', 'request']);
  }
}
