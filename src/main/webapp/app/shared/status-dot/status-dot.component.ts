import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'jhi-status-dot',
  templateUrl: './status-dot.component.html',
  styleUrls: ['./status-dot.component.scss']
})
export class StatusDotComponent implements OnInit {
  @Input() dotData = {
    color: '',
    class: '',
    tooltipText: '',
    tooltipPlacement: 'top'
  };

  constructor() { }

  ngOnInit(): void {
    if (!this.dotData.tooltipPlacement) {
      this.dotData.tooltipPlacement = 'top';
    }
  }

}
