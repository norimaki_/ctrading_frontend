import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import { Account } from 'app/core/user/account.model';
import { LoginService } from "app/core/login/login.service.mock";
import { AccountService } from "app/core/auth/account.service.mock";
import { ToastService } from "app/shared/bootstrap/toast/toast.service";
import { Router } from "@angular/router";
import { animate, style, transition, trigger } from "@angular/animations";
import Navbar from "./navbar";

@Component({
  selector: 'jhi-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  animations: [
    trigger('submenuShowUp',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0 }),
            animate('.1s ease-out',
              style({ opacity: 1 }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 1 }),
            animate('.1s ease-in',
              style({ opacity: 0 }))
          ]
        )
      ]
    )
  ]
})

export class NavbarComponent implements OnInit {
  user: Account | null = null;
  public isNavbarCollapsed = true;
  public currentNavbarItem = {};
  public currentNavbarItemParent = {};
  public dropdownLevel2 = null;
  public activeSubmenu = 1;
  public menu = Navbar;

  constructor(
    private loginService: LoginService,
    private accountService: AccountService,
    private toastService: ToastService,
    private router: Router,
    private eRef: ElementRef
  ) { }

  ngOnInit(): void {
    this.getAuth();
    this.getMenu();
  }

  logout(): void {
    window.console.log('LOGOUT');
    this.loginService.logout();
    this.getAuth();
  }

  getAuth(): void {
    this.accountService.getAuthenticationState().subscribe((_user) => {
      if (_user === null) this.router.navigate(['account/index']);
      this.user = _user;
    });
  }

  handleLink(item: any): void {
    this.closeSubmenus(item);
    if (item === undefined) return;
    if (item.url) {
      this.router.navigate([item.url]);
      this.isNavbarCollapsed = true;
      this.currentNavbarItem = item;
      this.currentNavbarItemParent = {};
      return;
    }
    if (item.submenu) {
      item.submenu.opened = !item.submenu.opened;
    }
  }

  handleSubLink(item: any): void {
    if (item === undefined) return;
    if (item.url) {
      this.router.navigate([item.url]);
      this.isNavbarCollapsed = true;
      this.currentNavbarItemParent = this.menu.find((_item: any) => {
        if (_item.submenu === undefined) return;
        return _item.submenu.opened === true
      });
      this.currentNavbarItemParent.submenu.opened = false;
      this.currentNavbarItem = item;
      return;
    }
    if (item.submenu) {
      item.submenu.opened = !item.submenu.opened;
      this.dropdownLevel2 = item.submenu.nodes;
      this.activeSubmenu = 2;
    }
  }

  submenuBack(): void {
    this.activeSubmenu = 1;
    this.dropdownLevel2 = null;
  }

  getMenu(): void {
    this.currentNavbarItemParent = this.menu.find((_item: any) => {
      if (_item.submenu !== undefined) {
        return _item.submenu.nodes.find((_subitem: any) => {
          if (_subitem.url === this.router.url) {
            this.currentNavbarItem = _subitem;
            return true;
          }
        })
      }
      return _item.url === this.router.url;
    });
  }

  closeSubmenus(item: any): void {
    this.menu
      .filter((_item: any) => _item.id !== item.id)
      .filter((_item: any) => _item.submenu)
      .map((_item: any) => {
        if (_item.submenu === undefined) return;
        _item.submenu.opened = false;
      });
    this.activeSubmenu = 1;
  }

  @HostListener('document:click', ['$event'])
  clickout(event: Event):void {
    if(!this.eRef.nativeElement.contains(event.target)) {
      this.closeSubmenus({});
    }
  }

  showToast(): void {
    this.toastService.showToast('I am a standard toast', {
      delay: 5000,
      autohide: true
    });
    this.toastService.showSuccessToast('I am a success toast', {
      delay: 5000,
      autohide: true
    });
    this.toastService.showAlertToast('I am a alert toast', {
      delay: 5000,
      autohide: true
    });
    this.toastService.showErrorToast('I am a error toast', {
      delay: 5000,
      autohide: true
    });
  }
}
