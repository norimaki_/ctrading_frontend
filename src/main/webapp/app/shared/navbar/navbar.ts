export default [
  {
    'id': 'dashboard',
    'name': 'NAVBAR.MAIN.DASHBOARD',
    'url': '/dashboard',
    'icon': 'far fa-compass'
  },
  {
    'id': 'trading',
    'name': 'NAVBAR.MAIN.TRADING',
    'url': '/trading',
    'icon': 'fas fa-exchange-alt'
  },
  {
    'id': 'test',
    'name': 'Test',
    'icon': 'fas fa-charging-station',
    'submenu': {
      'opened': false,
      'nodes': [
        {
          'id': 'buy',
          'name': 'NAVBAR.MAIN.BUY',
          'url': '/buy',
          'icon': 'fas fa-money-bill-wave'
        },
        {
          'id': 'arbitration',
          'name': 'NAVBAR.MAIN.ARBITRATION',
          'url': '/arbitration',
          'icon': 'fas fa-coins'
        },
        {
          'id': 'subitem2',
          'name': 'Parent',
          'icon': 'fas fa-keyboard',
          'submenu': {
            'opened': false,
            'nodes': [
              {
                'id': 'subitem11',
                'name': 'child 1',
                'url': '/trading',
                'icon': 'fas fa-quote-right'
              },
              {
                'id': 'subitem12',
                'name': 'Child 2',
                'url': '/trading',
                'icon': 'fas fa-keyboard',
              },
              {
                'id': 'subitem13',
                'name': 'Child 1',
                'url': '/trading',
                'icon': 'fas fa-quote-right'
              },
              {
                'id': 'subitem14',
                'name': 'Child 3',
                'url': '/trading',
                'icon': 'fas fa-keyboard',
              },
              {
                'id': 'subitem15',
                'name': 'Child 4',
                'url': '/trading',
                'icon': 'fas fa-quote-right'
              },
              {
                'id': 'subitem22',
                'name': 'Child 5',
                'url': '/trading',
                'icon': 'fas fa-keyboard',
              }
            ]
          }
        },
        {
          'id': 'subitem2',
          'name': 'Parent 2',
          'icon': 'fas fa-keyboard',
          'submenu': {
            'opened': false,
            'nodes': [
              {
                'id': 'subitem21',
                'name': 'Child 21',
                'url': '/trading',
                'icon': 'fas fa-keyboard',
              },
              {
                'id': 'subitem22',
                'name': 'Child 22',
                'url': '/trading',
                'icon': 'fas fa-quote-right'
              },
              {
                'id': 'subitem23',
                'name': 'Child 23',
                'url': '/trading',
                'icon': 'fas fa-keyboard',
              }
            ]
          }
        }
      ]
    }
  },
  {
    'id': 'portfolio',
    'name': 'NAVBAR.MAIN.PORTFOLIO',
    'url': '/portfolio',
    'icon': 'fas fa-sync-alt'
  },
  {
    'id': 'admin',
    'name': 'NAVBAR.MAIN.ADMIN',
    'url': '/admin/user-management',
    'icon': 'fas fa-user'
  }
];
