import { Component, Input } from '@angular/core';
import { Router } from "@angular/router";
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'jhi-back-button',
  templateUrl: './back-button.component.html'
})
export class BackButtonComponent {
  @Input() backLink = '/';
  faAngleLeft = faAngleLeft;

  constructor(
    private router: Router
  ) {}

  back(): void {
    this.router.navigate([this.backLink]);
  }
}
