import { Component, HostBinding, TemplateRef } from '@angular/core';
import { ToastService } from "./toast.service";
import { animate, style, transition, trigger } from "@angular/animations";

@Component({
  selector: 'jhi-toast',
  templateUrl: './toast.component.html',
  animations: [
    trigger('toast',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0 }),
            animate('1s ease-out',
              style({ opacity: 1 }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 1 }),
            animate('1s ease-in',
              style({ opacity: 0 }))
          ]
        )
      ]
    )
  ]
})
export class ToastComponent {
  @HostBinding('class.ngb-toasts')
  public toasts: any[] = [];

  constructor(
    public toastService: ToastService
  ) {}

  isTemplate(toast: object): string | TemplateRef<any> {
    return toast.textOrTpl instanceof TemplateRef;
  }
}
