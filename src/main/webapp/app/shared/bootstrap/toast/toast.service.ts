import { Injectable, TemplateRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  public toasts: any[] = [];

  constructor() { }

  showSuccessToast(textOrTpl: string | TemplateRef<any>, options: any = {}): void {
    options['classname'] = 'bg-success text-light';
    this.showToast(textOrTpl, options);
  }

  showAlertToast(textOrTpl: string | TemplateRef<any>, options: any = {}): void {
    options['classname'] = 'bg-primary text-light';
    this.showToast(textOrTpl, options);
  }

  showErrorToast(textOrTpl: string | TemplateRef<any>, options: any = {}): void {
    options['classname'] = 'bg-danger text-light';
    this.showToast(textOrTpl, options);
  }

  showToast(textOrTpl: string | TemplateRef<any>, options: any = { delay: 5000, autohide: true }): void {
    this.toasts.push({ textOrTpl, ...options });
  }

  remove(toast: any): void {
    this.toasts = this.toasts.filter(t => t !== toast);
  }
}
