export interface ModalModel {
  title?: string | null;
  contentText?: string;
  contentHtml?: string;
  acceptButton?: string;
  cancelButton?: string | null;
  hideClose?: boolean
}

export class Modal implements ModalModel {
  constructor (
    public title?: string | null,
    public contentText?: string,
    public contentHtml?: string,
    public acceptButton?: string,
    public cancelButton?: string | null,
    public hideClose?: boolean
  ) {}
}
