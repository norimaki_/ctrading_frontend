import { Injectable } from '@angular/core';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from "./modal.component";

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  constructor(private ngbModal: NgbModal) {}

  public openModal(options: any = {}): Promise<NgbModalRef> {
    const modalOptions: NgbModalOptions = {
      backdrop: 'static'
    };
    if ('type' in options) {
      modalOptions.windowClass = `modal-type-${options.type}`;
    }
    const modalRef: NgbModalRef = this.ngbModal.open(ModalComponent, modalOptions);
    modalRef.componentInstance.modalParams = options;
    return modalRef.result;
  }
}
