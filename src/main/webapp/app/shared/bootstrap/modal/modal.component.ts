import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { Modal } from "./modal.model";

@Component({
  selector: 'jhi-modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent implements OnInit {
  @Input() modalParams: Modal = {};
  faTimes = faTimes;

  constructor(
    public activeModal: NgbActiveModal
  ) {}

  ngOnInit(): void {
    if (typeof this.modalParams.acceptButton === 'undefined') {
      this.modalParams.acceptButton = `Accept`;
    }
  }
}
