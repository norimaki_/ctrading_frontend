import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'jhi-login-modal',
  templateUrl: './login-modal.component.html'
})
export class LoginModalComponent {
  faTimes = faTimes;

  constructor(
    public activeModal: NgbActiveModal
  ) {}
}
