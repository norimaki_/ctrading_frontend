import { NgModule } from '@angular/core';
import { CtradingClientSharedLibsModule } from './shared-libs.module';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { LoginModalComponent } from './login-modal/login-modal.component';
import { LoginFormComponent } from "./login-form/login-form.component";
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { BackButtonComponent } from "./back-button/back-button.component";
import { StatusDotComponent } from "./status-dot/status-dot.component";
import { UiSwitchModule } from 'ngx-toggle-switch';
import { NgxPermissionsModule } from "ngx-permissions";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ToastComponent } from "./bootstrap/toast/toast.component";
import { ModalComponent } from "./bootstrap/modal/modal.component";
import { PairListComponent } from "./pair-list/pair-list.component";
import { OrderBookComponent } from "./order-book/order-book.component";
import { ChartComponent } from "./chart/chart.component";
import { DataTableModule } from "../data-table/data-table.module";
import { TradeDetailComponent } from './trade-detail/trade-detail.component';
import { ThemeSwitchComponent } from './theme-switch/theme-switch.component';
import { TranslationComponent } from './translation/translation.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CtradingClientSharedLibsModule,
    UiSwitchModule,
    NgxPermissionsModule.forRoot(),
    NgbModule,
    DataTableModule,
    TranslateModule
  ],
  declarations: [
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    LoginFormComponent,
    HasAnyAuthorityDirective,
    BackButtonComponent,
    StatusDotComponent,
    ToastComponent,
    ModalComponent,
    PairListComponent,
    OrderBookComponent,
    ChartComponent,
    TradeDetailComponent,
    ThemeSwitchComponent,
    TranslationComponent
  ],
  entryComponents: [LoginModalComponent, ModalComponent, TradeDetailComponent],
  exports: [
    CtradingClientSharedLibsModule,
    UiSwitchModule,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    LoginFormComponent,
    HasAnyAuthorityDirective,
    BackButtonComponent,
    StatusDotComponent,
    NgxPermissionsModule,
    NgbModule,
    ToastComponent,
    ModalComponent,
    PairListComponent,
    OrderBookComponent,
    ChartComponent,
    TradeDetailComponent,
    ThemeSwitchComponent,
    TranslationComponent
  ]
})
export class CtradingClientSharedModule {}
