import { Component, AfterViewChecked, OnInit, Input } from '@angular/core';
import { ThemeService } from 'app/theme/theme.service';
import { Chart } from "chart.js";
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Theme } from 'app/theme/theme.model';
import { chartColors } from 'app/shared/chart/chart.model';
import { lightTheme } from 'app/theme/light-theme';
import { darkTheme } from 'app/theme/dark-theme';

@Component({
  selector: 'jhi-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements AfterViewChecked, OnInit {
  @Input() chartData = {};
  private unsubscribe = new Subject();
  public chart: any = null;
  chartLoaded = false;
  chartSet = false;
  borderColor = lightTheme.properties['--card-background'];
  backgroundColors = new Array(0);
  chartId = Date.now() * Math.round(Math.random() * (1 - 1000000) + 1000000);

  constructor(
    private _themeService: ThemeService
  ) {}

  ngOnInit(): void {
    const activeTheme = this._themeService.getActiveTheme();
    this.setChartTheme(activeTheme);
    this._themeService.themeChange.pipe(takeUntil(this.unsubscribe))
      .subscribe((theme: Theme) => {
        this.setChartTheme(theme);
        this.setChartBorders();
      });
  }

  ngAfterViewChecked(): void {
    if (this.chartLoaded) return;
    if (this.chartData === undefined) return;
    if (this.chartData.data === undefined) return;
    if (!this.chartSet) {
      this.setChartConfig();
    }
    this.fetchChartData();
  }

  setChartConfig(): void {
    this.chartSet = false;
    if (this.chartData === undefined) return;
    this.backgroundColors = new Array(this.chartData.data.labels.length)
      .fill(0)
      .map((_, i: number) => {
        return (chartColors[i] === undefined) ? '#ff0000' : chartColors[i]
      });
    if (this.chartData.data.datasets[0].backgroundColor === undefined) {
      Object.assign(this.chartData.data.datasets[0], { backgroundColor: this.backgroundColors });
    }
    if (this.chartData.data.datasets[0].borderWidth === undefined) {
      Object.assign(this.chartData.data.datasets[0], { borderWidth: 2 });
    }
    Object.assign(this.chartData.data.datasets[0], { borderColor: new Array(this.chartData.data.labels.length).fill(this.borderColor) });
    this.chartSet = true;
  }

  setChartBorders(): void {
    this.chartData.data.datasets[0].borderColor = new Array(this.chartData.data.labels.length).fill(this.borderColor);
    this.chartData.data.datasets[0].backgroundColor = this.backgroundColors;
    this.chartLoaded = false;
  }

  fetchChartData(): void {
    if (!this.chartSet) return;
    if (this.chartData === undefined) return;
    this.chartLoaded = false;
    const chartElement = document.getElementById(this.chartId + '-chart') as HTMLCanvasElement;
    if (chartElement === null) return;
    this.chart = new Chart(chartElement, {
      type: (this.chartData.type === undefined) ? 'doughnut': this.chartData.type,
      data: this.chartData.data,
      options: (this.chartData.options === undefined) ? {} : this.chartData.options
    });
    this.chartLoaded = true;
  }

  setChartTheme(theme: Theme): void {
    this.borderColor = (theme.name === 'light') ? lightTheme.properties['--card-background'] : darkTheme.properties['--card-background'];
  }
}
