import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Modal } from 'app/shared/bootstrap/modal/modal.model';

@Component({
  selector: 'jhi-trade-detail',
  templateUrl: './trade-detail.component.html',
  styleUrls: ['./trade-detail.component.scss']
})
export class TradeDetailComponent implements OnInit {
  @Input() trade: Modal = {};

  constructor(
    public activeModal: NgbActiveModal
  ) {}

  ngOnInit(): void {
    window.console.log("this.trade *****", this.trade);
  }

}
