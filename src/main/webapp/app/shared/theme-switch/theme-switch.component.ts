import { Component, HostBinding, OnInit } from '@angular/core';
import { ThemeService } from 'app/theme/theme.service';

@Component({
  selector: 'jhi-theme-switch',
  templateUrl: './theme-switch.component.html',
  styleUrls: ['./theme-switch.component.scss']
})
export class ThemeSwitchComponent implements OnInit {
  @HostBinding('class.dark-mode') darkMode = false;

  constructor(
    private themeService: ThemeService
  ) {}

  ngOnInit(): void {
    const active = this.themeService.getActiveTheme();
    this.darkMode = (active.name === 'dark') ? true : false;
  }

  onChangeEvent(): void {
    const active = this.themeService.getActiveTheme();
    this.darkMode = (active.name === 'dark') ? true : false;
    if (active.name === 'light') {
      this.themeService.setTheme('dark');
    } else {
      this.themeService.setTheme('light');
    }
  }
}
