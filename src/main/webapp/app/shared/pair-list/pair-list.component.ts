import {Component, EventEmitter, OnInit, OnChanges, Input, Output, HostListener} from '@angular/core';
import { DataTable } from "app/data-table/data-table.model";

@Component({
  selector: 'jhi-pair-list',
  templateUrl: './pair-list.component.html',
  styleUrls: ['./pair-list.component.scss']
})
export class PairListComponent implements OnInit, OnChanges {
  @Input() tableData = new DataTable();
  @Output() pairClickHandle: EventEmitter<any> = new EventEmitter();
  pairListTableData = new DataTable();
  pairSelected = '';
  pairListMaxHeight = 'auto';

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    this.pairListTableData = Object.assign({}, this.tableData);
    this.resizeBoxes();
  }

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    this.resizeBoxes();
  }

  resizeBoxes(): void {
    const buySellForm = document.getElementById('buysell-form');
    if (buySellForm === null) return;
    this.pairListMaxHeight = `${buySellForm.offsetHeight}px`;
  }

  rowClick(row: []): void {
    this.pairClickHandle.emit(row);
  }

  filterList(event: Event): void {
    if (event.target === null) return;
    this.pairListTableData.data = (event.target.value.length <= 0) ? this.tableData.data : this.tableData.data.filter(_item => _item[0].toLowerCase().includes(event.target.value.toLowerCase()));
  }
}
