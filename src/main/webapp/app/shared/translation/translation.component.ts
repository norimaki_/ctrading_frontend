import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-translation',
  templateUrl: './translation.component.html',
  styleUrls: ['./translation.component.scss']
})
export class TranslationComponent implements OnInit {
  public activeLang = 'en';

  constructor(
    private translate: TranslateService
  ) {
    translate.addLangs(['en', 'es']);
    translate.setDefaultLang(this.activeLang);
    const browserLang = translate.getBrowserLang();
    const regExp = /en|es/;
    this.activeLang = regExp.exec(browserLang) ? browserLang : this.activeLang;
    translate.use(this.activeLang);
  }

  ngOnInit(): void {}

  public setLanguage(lang: string): void {
    this.activeLang = lang;
    this.translate.use(lang);
  }
}
