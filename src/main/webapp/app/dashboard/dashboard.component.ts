import { Component, OnInit } from '@angular/core';
import { NgxPermissionsService, NgxRolesService } from "ngx-permissions";
import { AccountService } from "app/core/auth/account.service.mock";
import { Router } from "@angular/router";
import { Account } from "app/core/user/account.model";
import { DataTable } from "app/data-table/data-table.model";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  user: Account | null = null;

  dataResume = new DataTable();
  openedPositions = new DataTable();
  marketShare = new DataTable();
  lastTrades = new DataTable();
  balanceRoi = new DataTable();
  translations = {
    price: '',
    type: '',
    exchange: '',
    market: '',
    amount: '',
    date: '',
    detail: '',
  }

  constructor(
    private _permissionsService: NgxPermissionsService,
    private _rolesService: NgxRolesService,
    private _accountService: AccountService,
    private _router: Router,
    private _translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.getAuth();
    this.setTranslations();
  }

  getAuth(): void {
    this._accountService.getAuthenticationState().subscribe(_user => {
      if (_user === null) {
        this._router.navigate(['account/index']);
        return;
      }
      this.user = _user;
      this._permissionsService.loadPermissions(this.user.authorities);
    });
  }

  setTranslations(): void {
    this._translate.stream([
      'TABLE.HEADER.PRICE',
      'TABLE.HEADER.TYPE',
      'TABLE.HEADER.EXCHANGE',
      'TABLE.HEADER.MARKET',
      'TABLE.HEADER.AMOUNT',
      'TABLE.HEADER.DATE',
      'TABLE.HEADER.DETAIL',
    ]).subscribe(translation => {
      this.translations = {
        price: translation['TABLE.HEADER.PRICE'],
        type: translation['TABLE.HEADER.TYPE'],
        exchange: translation['TABLE.HEADER.EXCHANGE'],
        market: translation['TABLE.HEADER.MARKET'],
        amount: translation['TABLE.HEADER.AMOUNT'],
        date: translation['TABLE.HEADER.DATE'],
        detail: translation['TABLE.HEADER.DETAIL']
      }
      this.fetchData();
    })
  }

  fetchData(): void {
    setTimeout(() => {
      this.dataResume = {
        id: 'portfolio-resume',
        showHeader: false,
        cols: [
          {
            type: 'label',
            name: 'Initials'
          },
          {
            type: 'string',
            name: 'Currency'
          },
          {
            type: 'percentage',
            name: 'Percent',
            align: 'right'
          }
        ],
        data: [
          ['BTC', 'Bitcoin', '22'],
          ['XRP', 'Ripple', '39'],
          ['LTC', 'Litecoin', '39'],
          ['BTC', 'Bitcoin', '22'],
          ['XRP', 'Ripple', '39'],
          ['LTC', 'Litecoin', '39'],
          ['BTC', 'Bitcoin', '22'],
          ['XRP', 'Ripple', '39'],
          ['LTC', 'Litecoin', '39'],
          ['BTC', 'Bitcoin', '22'],
          ['XRP', 'Ripple', '39'],
          ['LTC', 'Litecoin', '39']
        ],
        dataChart: {
          id: 'portfolio-resume',
          type: 'doughnut',
          data: {
            labels: ['Bitcoin', 'Ripple', 'Litecoin'],
            datasets: [{
              label: 'Exchanges',
              data: [22, 39, 22]
            }]
          },
          options: {
            legend: {
              display: false
            }
          }
        }
      };
    }, 5000);

    setTimeout(() => {
      this.openedPositions = {
        id: 'opened-positions',
        showHeader: false,
        cols: [
          {
            type: 'string',
            name: 'Position'
          },
          {
            type: 'percentage',
            name: 'Percent',
            align: 'right'
          }
        ],
        data: [
          ['Simples', '50'],
          ['Trailing Stop', '50'],
          ['Arbitrage', '0']
        ],
        dataChart: {
          id: 'opened-positions',
          data: {
            labels: ['Simples', 'Trailing Stop', 'Arbitrage'],
            datasets: [{
              label: 'Exchanges',
              data: [50, 50, 0]
            }]
          },
          options: {
            legend: {
              display: false
            }
          }
        }
      };

      this.balanceRoi = {
        id: 'balance-roi',
        showHeader: true,
        cols: [
          {
            type: 'label',
            name: ''
          },
          {
            type: 'string',
            name: ''
          },
          {
            type: 'number',
            name: this.translations.price,
            align: 'center'
          },
          {
            type: 'percentage',
            name: '24h',
            align: 'right'
          }
        ],
        data: [
          ['BTC', 'Bitcoin', 4.45689354, '22'],
          ['XRP', 'BitcoinSatos...', 4.45689354, '39'],
          ['LTC', 'Bitcoin Cash', 4.45689354, '39'],
          ['BSV', 'Ethereum', 4.45689354, '222'],
          ['ETH', 'Ripple', 4.45689354, '139'],
          ['LTC', 'Litecoin', 4.45689354, '39'],
          ['BTC', 'Bitcoin', 4.45689354, '22'],
          ['XRP', 'Ripple', 4.45689354, '39'],
          ['LTC', 'Litecoin', 4.45689354, '39'],
          ['BTC', 'Bitcoin', 4.45689354, '122'],
          ['XRP', 'Ripple', 4.45689354, '39'],
          ['LTC', 'Litecoin', 4.45689354, '39']
        ]
      };
    }, 3500);

    setTimeout(() => {
      this.marketShare = {
        id: 'market-share',
        showHeader: false,
        rowsHeight: 'sm',
        cols: [
          {
            type: 'label',
            name: 'Initials'
          },
          {
            type: 'string',
            name: 'Currency'
          },
          {
            type: 'percentage',
            name: 'Percent',
            align: 'right'
          }
        ],
        data: [
          ['BTC', 'Bitcoin', '22'],
          ['XRP', 'Ripple', '39'],
          ['LTC', 'Litecoin', '39'],
          ['BTC', 'Bitcoin', '22'],
          ['XRP', 'Ripple', '39'],
          ['LTC', 'Litecoin', '39'],
          ['BTC', 'Bitcoin', '22'],
          ['XRP', 'Ripple', '39'],
          ['LTC', 'Litecoin', '39'],
          ['BTC', 'Bitcoin', '22'],
          ['XRP', 'Ripple', '39'],
          ['LTC', 'Litecoin', '39']
        ]
      };
    }, 3800);

    setTimeout(() => {
      this.lastTrades = {
        id: 'last-trades',
        showHeader: true,
        cols: [
          {
            type: 'label',
            name: this.translations.type,
            align: 'center',
            labelColors: {
              'BUY': 'success',
              'SELL': 'danger'
            }
          },
          {
            type: 'string',
            name: this.translations.exchange,
            align: 'center'
          },
          {
            type: 'string',
            name: this.translations.market,
            align: 'center'
          },
          {
            type: 'number',
            name: this.translations.amount,
            align: 'center'
          },
          {
            type: 'number',
            name: this.translations.price,
            align: 'center'
          },
          {
            type: 'string',
            name: this.translations.date,
            align: 'center'
          },
          {
            type: 'options',
            name: this.translations.detail,
            align: 'center'
          }
        ],
        data: [
          ['BUY', 'Bitfinex', 'ETH-BTC', '0.23343434', '0.23343434', '24-03-30 23:59', {'View Detail': '/account/login', 'Compare': '/account/index'}],
          ['SELL', 'Bitfinex', 'ETH-BTC', '0.23343434', '0.23343434', '24-03-30 23:59', {'View Detail': '/account/login', 'Compare': '/account/index'}],
          ['BUY', 'Bitfinex', 'ETH-BTC', '0.23343434', '0.23343434', '24-03-30 23:59', {'View Detail': '/account/login', 'Compare': '/account/index'}],
          ['BUY', 'Bitfinex', 'ETH-BTC', '0.23343434', '0.23343434', '24-03-30 23:59', {'View Detail': '/account/login', 'Compare': '/account/index'}],
          ['BUY', 'Bitfinex', 'ETH-BTC', '0.23343434', '0.23343434', '24-03-30 23:59', {'View Detail': '/account/login', 'Compare': '/account/index'}],
          ['BUY', 'Bitfinex', 'ETH-BTC', '0.23343434', '0.23343434', '24-03-30 23:59', {'View Detail': '/account/login', 'Compare': '/account/index'}],
          ['SELL', 'Bitfinex', 'ETH-BTC', '0.23343434', '0.23343434', '24-03-30 23:59', {'View Detail': '/account/login', 'Compare': '/account/index'}],
          ['BUY', 'Bitfinex', 'ETH-BTC', '0.23343434', '0.23343434', '24-03-30 23:59', {'View Detail': '/account/login', 'Compare': '/account/index'}],
        ]
      };
    }, 1000);
  }
}
