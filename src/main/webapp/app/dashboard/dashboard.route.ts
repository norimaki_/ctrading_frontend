import { Routes } from '@angular/router';
import { DashboardComponent } from "./dashboard.component";

export const dashboardState: Routes = [
  {
    path: '',
    component: DashboardComponent
  }
];
