import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from "@angular/router";
import { dashboardState } from "./dashboard.route";
import { CtradingClientSharedModule } from "app/shared/shared.module";
import { DataTableModule } from "app/data-table/data-table.module";
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(dashboardState),
    CtradingClientSharedModule,
    DataTableModule,
    TranslateModule
  ]
})
export class DashboardModule { }
