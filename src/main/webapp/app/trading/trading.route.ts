import { Routes } from '@angular/router';
import { TradingComponent } from "./trading.component";

export const tradingState: Routes = [
  {
    path: '',
    component: TradingComponent
  }
];
