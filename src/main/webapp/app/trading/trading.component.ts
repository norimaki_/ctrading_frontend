import { Component, OnInit } from '@angular/core';
import { DataTable } from "app/data-table/data-table.model";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-trading',
  templateUrl: './trading.component.html',
  styleUrls: ['./trading.component.scss']
})
export class TradingComponent implements OnInit {
  currentTab = 'byCurrency';
  byCurrencyTable = new DataTable();
  byExchangeTable = new DataTable();
  cryptocurrencyResume = new DataTable();
  exchangeResume = new DataTable();
  translations = {
    amount: '',
    cryptocurrency: '',
    price: '',
    portfolio: '',
    exchange: ''
  }

  constructor(
    private _translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.setTranslations();
  }

  setTranslations(): void {
    this._translate.stream([
      'TABLE.HEADER.AMOUNT',
      'TABLE.HEADER.CRYPTOCURRENCY',
      'TABLE.HEADER.PRICE',
      'TABLE.HEADER.PORTFOLIO',
      'TABLE.HEADER.EXCHANGE'
    ]).subscribe(translation => {
      this.translations = {
        amount: translation['TABLE.HEADER.AMOUNT'],
        cryptocurrency: translation['TABLE.HEADER.CRYPTOCURRENCY'],
        price: translation['TABLE.HEADER.PRICE'],
        portfolio: translation['TABLE.HEADER.PORTFOLIO'],
        exchange: translation['TABLE.HEADER.EXCHANGE']
      }
      this.fetchData();
    })
  }

  openTab(tab: string): void {
    this.currentTab = tab;
  }

  fetchData(): void {
    setTimeout(() => {
      this.byCurrencyTable = {
        id: 'by-currency-table',
        showHeader: true,
        cols: [
          {
            type: 'icon',
            name: ''
          },
          {
            type: 'string',
            name: this.translations.cryptocurrency
          },
          {
            type: 'number',
            name: this.translations.amount,
            align: 'center'
          },
          {
            type: 'price',
            name: this.translations.price,
            currency: 'USD',
            align: 'center'
          },
          {
            type: 'percentage',
            name: `% ${this.translations.portfolio}`,
            progressbar: true
          },
          {
            type: 'string',
            name: this.translations.exchange,
            align: 'right'
          }
        ],
        data: [
          ['/content/images/cryptocurrencies/Bitcoin.png', 'Bitcoin', 13.83, 4.455, '+29', 'Bitfinex'],
          ['/content/images/cryptocurrencies/Ethereum.png', 'Ethereum', 64.4, 4.455, '+32', 'Bitfinex'],
          ['/content/images/cryptocurrencies/Litecoin.png', 'Litecoin', 3.4324, 86.3, '+85', 'Hitbtc'],
          ['/content/images/cryptocurrencies/Bitcoin.png', 'Bitcoin', 13.83, 4.455, '-49', 'Bitfinex'],
          ['/content/images/cryptocurrencies/Ethereum.png', 'Ethereum', 64.4, 4.455, '+12', 'Bitfinex'],
          ['/content/images/cryptocurrencies/Litecoin.png', 'Litecoin', 3.4324, 86.3, '+36', 'Hitbtc'],
          ['/content/images/cryptocurrencies/Bitcoin.png', 'Bitcoin', 13.83, 4.455, '+96', 'Bitfinex'],
          ['/content/images/cryptocurrencies/Ethereum.png', 'Ethereum', 64.4, 4.455, '+232', 'Bitfinex'],
          ['/content/images/cryptocurrencies/Litecoin.png', 'Litecoin', 3.4324, 86.3, '+11', 'Hitbtc'],
          ['/content/images/cryptocurrencies/Bitcoin.png', 'Bitcoin', 13.83, 4.455, '+29', 'Bitfinex'],
          ['/content/images/cryptocurrencies/Ethereum.png', 'Ethereum', 64.4, 4.455, '+32', 'Bitfinex'],
          ['/content/images/cryptocurrencies/Litecoin.png', 'Litecoin', 3.4324, 86.3, '+1', 'Hitbtc']
        ]
      };
    }, 1500);

    setTimeout(() => {
      this.byExchangeTable = {
        id: 'by-exchange-table',
        showHeader: true,
        cols: [
          {
            type: 'string',
            name: this.translations.exchange
          },
          {
            type: 'number',
            name: this.translations.amount,
            align: 'center'
          },
          {
            type: 'price',
            name: this.translations.price,
            currency: 'USD',
            align: 'center'
          },
          {
            type: 'percentage',
            name: `% ${this.translations.portfolio}`,
            progressbar: true
          },
          {
            type: 'string',
            name: this.translations.cryptocurrency,
            icon: 'true',
            align: 'right'
          },
          {
            type: 'icon',
            name: ''
          }
        ],
        data: [
          ['Bitfinex', 13.83, 4.455, '+29', 'Bitcoin', '/content/images/cryptocurrencies/Bitcoin.png'],
          ['Bitfinex', 64.4, 4.455, '+32', 'Ethereum', '/content/images/cryptocurrencies/Ethereum.png'],
          ['Hitbtc', 3.4324, 86.3, '+85', 'Litecoin', '/content/images/cryptocurrencies/Litecoin.png'],
          ['Bitfinex', 13.83, 4.455, '-49', 'Bitcoin', '/content/images/cryptocurrencies/Bitcoin.png'],
          ['Bitfinex', 64.4, 4.455, '+12', 'Ethereum', '/content/images/cryptocurrencies/Ethereum.png'],
          ['Hitbtc', 3.4324, 86.3, '+36', 'Litecoin', '/content/images/cryptocurrencies/Litecoin.png'],
          ['Bitfinex', 13.83, 4.455, '+96', 'Bitcoin', '/content/images/cryptocurrencies/Bitcoin.png'],
          ['Bitfinex', 64.4, 4.455, '+232', 'Ethereum', '/content/images/cryptocurrencies/Ethereum.png'],
          ['Hitbtc', 3.4324, 86.3, '+11', 'Litecoin', '/content/images/cryptocurrencies/Litecoin.png'],
          ['Bitfinex', 13.83, 4.455, '+29', 'Bitcoin', '/content/images/cryptocurrencies/Bitcoin.png'],
          ['Bitfinex', 64.4, 4.455, '+32', 'Ethereum', '/content/images/cryptocurrencies/Ethereum.png'],
          ['Hitbtc', 3.4324, 86.3, '+1', 'Litecoin', '/content/images/cryptocurrencies/Litecoin.png']
        ]
      };
    }, 1800);

    setTimeout(() => {
      this.cryptocurrencyResume = {
        id: 'cryptocurrency-resume',
        cols: [
          {
            type: 'label',
            name: this.translations.exchange
          },
          {
            type: 'string',
            name: this.translations.cryptocurrency
          },
          {
            type: 'percentage',
            name: `% ${this.translations.portfolio}`,
            align: 'right'
          }
        ],
        data: [
          ['BTC', 'Bitcoin', '+29'],
          ['XRP', 'Ethereum', '+32'],
          ['LTC', 'Litecoin', '+85'],
          ['BTC', 'Bitcoin', '+29'],
          ['XRP', 'Ethereum', '+32'],
          ['LTC', 'Litecoin', '+85'],
          ['BTC', 'Bitcoin', '+29'],
          ['XRP', 'Ethereum', '+32'],
          ['LTC', 'Litecoin', '+85'],
          ['BTC', 'Bitcoin', '+29'],
          ['XRP', 'Ethereum', '+32'],
          ['LTC', 'Litecoin', '+85']
        ],
        dataChart: {
          id: 'cryptocurrency-resume',
          data: {
            labels: ['Bitcoin', 'Ethereum', 'Litecoin'],
            datasets: [{
              label: 'Exchanges',
              data: [22, 32, 46],
              backgroundColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
              ],
              borderColor: [
                'rgba(255, 255, 255, 1)',
                'rgba(255, 255, 255, 1)',
                'rgba(255, 255, 255, 1)'
              ],
              borderWidth: 2
            }]
          },
          options: {
            legend: {
              display: false
            }
          }
        }
      };
    }, 1200);

    setTimeout(() => {
      this.exchangeResume = {
        id: 'exchange-resume',
        cols: [
          {
            type: 'string',
            name: this.translations.cryptocurrency
          },
          {
            type: 'percentage',
            name: `% ${this.translations.portfolio}`,
            align: 'right'
          }
        ],
        data: [
          ['Bitfinex', '+29'],
          ['Binance', '+32'],
          ['Bitrex', '+85'],
          ['Hitbtc', '+29'],
          ['Poloniex', '+32'],
          ['Kraken', '+85'],
          ['Gemini', '+29'],
          ['Bitmex', '+32'],
          ['Coinbase Pro', '+85'],
          ['Kucoin', '+29'],
          ['Lbank', '+32'],
          ['BTCturk', '+85']
        ],
        dataChart: {
          id: 'exchange-resume',
          data: {
            labels: ['Bitfinex', 'Binance', 'Bitrex', 'Hitbtc', 'Poloniex', 'Kraken', 'Gemini', 'Bitmex', 'Coinbase Pro', 'Kucoin', 'Lbank', 'BTCturk'],
            datasets: [{
              label: 'Exchanges',
              data: [22, 32, 85, 29, 32, 85, 29, 32, 85, 29, 32, 85],
              backgroundColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
              ],
              borderColor: [
                'rgba(255, 255, 255, 1)',
                'rgba(255, 255, 255, 1)',
                'rgba(255, 255, 255, 1)'
              ],
              borderWidth: 2
            }]
          },
          options: {
            legend: {
              display: false
            }
          }
        }
      };
    }, 2500);
  }
}
