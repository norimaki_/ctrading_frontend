import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { CtradingClientSharedModule } from "app/shared/shared.module";
import { TradingComponent } from './trading.component';
import { tradingState } from './trading.route';
import { DataTableModule } from "app/data-table/data-table.module";
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [TradingComponent],
  imports: [
    CommonModule,
    CtradingClientSharedModule,
    RouterModule.forChild(tradingState),
    DataTableModule,
    TranslateModule
  ]
})
export class TradingModule { }
